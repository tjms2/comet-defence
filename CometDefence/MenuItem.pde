abstract class MenuItem {
  PVector centrePos;
  
  //optional
  int red;
  int green;
  int blue;
  boolean colDefined;
  
  MenuItem(PVector p) {
   centrePos = p; 
  }
  
  MenuItem(PVector p, int r, int gr, int bl) {
   centrePos = p; 
   colDefined = true; 
   red = r;
   green = gr;
   blue = bl;
  }
  abstract void render(); 
  
  
  
  int Red() {
    return colDefined ? red : 150; 
  }
  
  int Green() {
    return colDefined ? green : 150; 
  }
  
  int Blue() {
    return colDefined ? blue : 150; 
  }
}