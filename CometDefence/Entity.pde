public abstract class Entity {
  
  PVector position;
  PVector velocity;
  PVector acceleration;//reset every frame
  PVector forces;//reset every frame USUALLY
  
  float mass;
  float radius;
  boolean affectedByGravity;//constant
  boolean affectedByDrag;//multiplier
  boolean affectedByCollisions;
  boolean affectedByExplosions;
  boolean enabled;
  
  void enable(){
    enabled = true;
  }
  
  void disable(){
    enabled = false;
  }
  
  abstract void render();
  abstract void runScripts();
  
  
  //applys the acceleration to the velocity and also any hard coded effects
  public void updateVelocity() {
    if(enabled)
      velocity.add(acceleration);
  }
  
  //takes the forces and converts them to acceleration  
  public void calculateAcceleration() {
    if(enabled)
      acceleration = forces.copy().div(mass);
  }
  
  void resetForce() {
    if(enabled)
      forces = new PVector(0,0);
  }

  void updatePosition() {
    if(enabled)
      position.add(velocity);
  }

  boolean touching(PVector pos) {
    return position.copy().sub(pos).mag() <= radius;
  }

  boolean touching(Entity o, float rad) {
    float mindist = rad+o.radius;
    PVector relativePos = position.copy().sub(o.position);
    float dist = relativePos.mag();
    return (dist <= mindist);
  }

  boolean touching(Entity o) {
    float mindist = radius+o.radius;
    PVector relativePos = position.copy().sub(o.position);
    float dist = relativePos.mag();
    return (dist <= mindist);
  }
  
  boolean touchingAnything() {
    if (!affectedByCollisions) return false;
    for (int i = 0; i < objects.size(); i++) {
      if (touching(objects.get(i)) && this!= objects.get(i)) return true;
    }
    return false;
  }
  
  boolean touchingAnythingExceptGround() {
    for (int i = 0; i < objects.size(); i++) {
      if (touching(objects.get(i)) && this!= objects.get(i)) 
        if (objects.get(i) != ground)
          return true;
    }
    return false;
  }
  
  boolean touchingAnythingExceptCities() {
    for (int i = 0; i < objects.size(); i++) {
      if (touching(objects.get(i)) && this!= objects.get(i)) 
        if (objects.get(i).getClass() != City.class)
          return true;
    }
    return false;
  }
  
  boolean approaching(Entity o) {
    //this method tests moving forward a little bit and sees if the object is closer or further away afterwards. simple and works.
    PVector relativePos = position.copy().sub(o.position);
    PVector relativeVel = velocity.copy().sub(o.velocity);
    relativeVel.normalize();  
    relativeVel.mult(0.1);//small velocity
    PVector relativePosAfterVel = relativePos.copy().add(relativeVel);
    return relativePosAfterVel.mag() < relativePos.mag();
  }
  
  
  float inverseMass() {
   return 1f/mass; 
  }
  
  boolean awake() {
   return velocity.mag()>0.1; 
  }
}