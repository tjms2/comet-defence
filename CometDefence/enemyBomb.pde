class EnemyBomb extends Bomb {
 
  ArrayList<PVector> positions = new ArrayList<PVector>();//TODO
  int tailSize = 5;
  int tailCounter;
  int col;
  
  HomingThrust homingThrust;
  
   EnemyBomb(float rad, PVector pos, int colour) {
    affectedByGravity = true;
    affectedByCollisions = true;
    affectedByDrag = true;
    affectedByExplosions = true;
    position = pos;
    velocity = new PVector(0,1);
    radius = rad;
    mass = PI*radius*radius;
    if(colour == PINK) mass *=2.5;
    renderState = NORMALBOMB;
    col = colour;
    if(colour == BLUE){
      homingThrust = new EnemyHomingThrust(this, -0.025, radius*40);//needs to have negative power to avoid rather than home in
      activeForces.add(homingThrust);
    }
    if(colour == GREEN) {
      homingThrust = new CityHomingThrust(this, 0.025, radius*50);
      activeForces.add(homingThrust);
    }
    enabled = true;
  }
  
  void runScripts(){
    if(offScreenHorizontally(position, radius)) {
      objectsForDestruction.add(this);
      if(col == BLUE || col == GREEN) forcesResolved.add(homingThrust);
      gold+= value();
      score+= value();
      cometsDeterred ++;
      return;
    }
    if(renderState==NORMALBOMB)
      if(detonate() || exploded) {
        if(col == BLUE || col == GREEN) forcesResolved.add(homingThrust);
         for(int i = 0; i < objects.size(); i++ ){
           Entity o = objects.get(i);
           if(o.getClass() == City.class && o.enabled) 
             if(touching(o))
               ((City) o).damage();
        }
      }
    runBombExplosionScript();
  }
  boolean detonate() {
    return touching(ground);
  }
  
  void renderNormal() {
    ellipseMode(RADIUS);
    stroke(50);
    int numShadows = positions.size();
    for(int i = 0; i < numShadows; i++) {
      int transition = (numShadows-i)*150/numShadows + 50;
      float radiusOfFlame = (radius*1.5 * (i+6))/(numShadows+6);
      
      switch(col) {
        case RED:
        fill(240,transition,30);
        break;
        case BLUE:
        fill(30,transition,240);
        break;
        case GREEN:
        fill(transition,240,30);
        break;
        case PINK:
        fill(240,transition,240);
      }
      
      ellipse(positions.get(i).x, positions.get(i).y, radiusOfFlame,radiusOfFlame);
    }
    fill(20,27,15);
    ellipse(position.x, position.y, radius,radius);
    fill(130,87,0);
    ellipse(position.x-radius/4, position.y-radius/4, radius/2, radius/2);
  
  }
  
  void updatePosition() {
    tailCounter++;
    if(tailCounter==5) {
      if(positions.size() >= tailSize) {
        positions.remove(0);
      }
      positions.add(position.copy());
      tailCounter =0;
    }
    super.updatePosition();
  }
  
  int value() {
    switch(col) {
     case RED:
       return 10;
     case BLUE:
       return 20;
     case GREEN:
       return 30;
     case PINK:
       return 30;
     default:
       return 0;
    }
  }
  
}