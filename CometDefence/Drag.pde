class Drag extends ForceGenerator {
 
  void apply() {
    for(int i = 0; i < objects.size(); i ++) {
      Entity o = objects.get(i);
      if(o.affectedByDrag && o.enabled) {
        o.forces.add(dragForce(o)); 
      }
    } 
  }
  
  PVector dragForce(Entity o) {
    return o.velocity.copy().sub(o.velocity.copy().mult(getDragRate())).mult(-o.mass);
  }
}