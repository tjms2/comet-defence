class DefenceBomb extends Bomb {
  
  PVector cannonForce;
  float goal;
  float distanceTravelled;
  boolean shot;
  
  
  DefenceBomb(PVector thrust, PVector position, float distanceToMouse, float bombLevel, float r) {
    goal = distanceToMouse;
    affectedByGravity = true;
    affectedByDrag = true;
    affectedByCollisions = true;
    affectedByExplosions = true;
    this.position = position;
    velocity = new PVector(0,0);
    this.forces = new PVector(0,0);
    radius = r;
    mass = PI*radius;
    renderState = NORMALBOMB;
    cannonForce = thrust;
    this.bombLevel = bombLevel;
    enabled = true;
  }
  
  void runScripts(){
    runBombExplosionScript();
  }
  
  boolean detonate() {
    return touchingAnythingExceptCities() || distanceTravelled >= goal;
  }
  
  void renderNormal() {
    renderVanillaBomb();
  }
  
  void resetForce() {
    forces = new PVector(0,0);
    if(!shot) forces.add(cannonForce);
    shot = true;
  }
  
  void updatePosition() {
    distanceTravelled += velocity.mag();
    super.updatePosition();
  }
}