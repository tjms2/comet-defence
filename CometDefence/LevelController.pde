
final int LOADING = -1;
final int MAINMENU = 0;
final int LEVEL = 2;
final int UPGRADEMENU = 4;
final int GAMEOVER = 5;
final int PHYSICSSIMULATION = 6;

int gameState;
AudioPlayer currentSong;

int loadingTime;

int levelNumber;
int levelTimer;

//button flags
boolean mainMenuPlayPressed;
boolean clusterUpgraded;
boolean railUpgraded;
boolean homingUpgraded;
boolean powerUpgraded;
boolean ROFUpgraded;
boolean bombUpgraded;
boolean citiesRebuilt;
boolean upgradeMenuPlayPressed;

void setUpLevelController() {
  switchToPhysicsSimulation();
}
//gets run every frame. This controller is what provides the game with the "IOFL" structure
void runLevelController() {
  if(currentSong!=null)
    if(!currentSong.isPlaying()) {
      currentSong.rewind();
      currentSong.play();
    }
  switch(gameState) {
   case LOADING:
     if(loadingTime >= maxLoad) {
       switchToMainMenu();
       break;
     }
     if(loadingTime == maxLoad -95) changeSong(infinitePowerDrop);
     renderLoadingScreen();
     loadingTime++;
     break;
   case MAINMENU:
     runMainMenuScripts();
     break;
   case LEVEL:
     runLevelScripts();
     break;
   case UPGRADEMENU:
     runUpgradeMenuScripts();
     break;
   case GAMEOVER:
     runGameOverScript();
     break;
   case PHYSICSSIMULATION:
  }
}

void setUpInitialGame() {
  levelNumber = 0;
  bullets = 100;
  bulletsFired = 0;
  cometsDeterred = 0;
  score = 0;
  gold = 0;
  powerUpgrade = 1;
  ROFUpgrade = 1;
  bombUpgrade = 1;
  clusterCannonUpgrade = 0;
  railCannonUpgrade = 0;
  homingCannonUpgrade = 0;
  ground = new Ground();
  cannon = new Cannon();
  objectsForCreation.add(ground);
  objectsForCreation.add(cannon);
  loadSound();
  setUpCities();
  setUpHUD();
  setUpPhysics();
}

int gameOverTimer;

void runGameOverScript() {
  if(gameOverTimer == 1400) {
     backGroundLine1 = null;
     backGroundLine2 = null;
     backGroundLine3 = null;
     backGroundLine4Centre = null;
     setUpInitialGame();
     switchToLoading  ();
     return;
  }
  gameOverTimer ++;
  if(gameOverTimer > 120) {
     backGroundLine1 = "GAME OVER";
     backGroundLine2 = "score: " + score;
     backGroundLine3 = "missiles fired: " + bulletsFired;
     backGroundLine4Centre = "comets deterred: " + cometsDeterred;
  }
}

void runMainMenuScripts() {
  renderMainMenu();
  if(mainMenuPlayPressed) {
    leaveMainMenu();
    switchToLevel();
  } else if(random(5) < 1) spawnRandomTimeBomb();
}

void runLevelScripts() {
  if(health==0) {
    leaveLevel();
    switchToGameOver();
  }
  renderHUD();
  levelTimer++;
  if(levelTimer == 2) enableCities();//prevents multi thread bug caused by first level starting before cities are finished being created/ needs to wait 1 frame
  switch(levelNumber) {
    case 0:
      runLevel0Scripts();
      break;
    case 1:
      runLevel1Scripts();
      break;
    case 2:
      runLevel2Scripts();
      break;
    case 3:
      runLevel3Scripts();
      break;
    case 4:
      runLevel4Scripts();
      break;
    default:
  }
}

void runUpgradeMenuScripts() {
  renderUpgradeMenu();
  renderHUD();
  
  if(clusterUpgraded) {
    clusterUpgraded = false;
    if(cannon.cannonType == CLUSTERCANNON) {
       cannon.clusterCannonLevel++;
    }
    cannon.cannonType = CLUSTERCANNON;
    gold -= upgradeToClusterCost.numericalContents;
    clusterCannonUpgrade++;
    updateUpgradeMenu();
  }
  if(railUpgraded) {
    railUpgraded = false;
    if(cannon.cannonType == RAILCANNON) {
       cannon.upgradeMissiles();
    } else {
       cannon.rateOfFireLevel *= 0.85;
       cannon.missileSize *= 0.6;
    }
    cannon.cannonType = RAILCANNON;
    gold -= upgradeToRailCost.numericalContents;
    railCannonUpgrade++;
    updateUpgradeMenu();
  }
  if(homingUpgraded) {
    homingUpgraded = false;
    if(cannon.cannonType == HOMINGCANNON) {
       cannon.upgradeHomingMissiles();
    }
    cannon.cannonType = HOMINGCANNON;
    gold -= upgradeToHomingCost.numericalContents;
    homingCannonUpgrade++;
    updateUpgradeMenu();
  }
  if(powerUpgraded) {
    powerUpgraded = false;
    gold-=powerUpgrade*100;
    powerUpgrade++;
    cannon.upgradePower();
    updateUpgradeMenu();
  }
  if(ROFUpgraded) {
    ROFUpgraded = false;
    gold-=ROFUpgrade*100;
    ROFUpgrade++;
    cannon.upgradeRateOfFire();
    updateUpgradeMenu();
  }
  if(bombUpgraded) {
    bombUpgraded = false;
    gold-=bombUpgrade*100;
    bombUpgrade++;
    cannon.upgradeMissileSize();
    updateUpgradeMenu();
  }
  if(citiesRebuilt) {
    citiesRebuilt = false;
    removeCities();
    setUpCities();
    gold -= 250;
    updateUpgradeMenu();
  }
  if(upgradeMenuPlayPressed) {
    upgradeMenuPlayPressed = false;
    switchToLevel();
  } 
}


void switchToPhysicsSimulation() {
  gameState=PHYSICSSIMULATION;
  for(int i = 0; i < 400; i++) {
    objectsForCreation.add(new Ball()); 
  }
}

void switchToLoading() {
  gameState = LOADING;
  loadingTime = 0;
}

void switchToMainMenu() {
 cannon.disable();
 disableCities();
 gameState = MAINMENU;
 mainMenuPlayPressed = false;
 changeSong(infinitePowerDrop);
}

void leaveMainMenu() {
  //remove the flying bombs
  for(int i = 0; i < objects.size(); i++) 
    if(objects.get(i).getClass() == TimeBomb.class) 
      objectsForDestruction.add(objects.get(i));
  //remove the explosions caused by flying bombs
  removeExplosions();
  infinitePowerDrop.pause();
  infinitePowerDrop.rewind();
  changeSong(null);
}

void removeExplosions() {
  for(int i = 0; i < activeForces.size(); i++)
    if(activeForces.get(i).getClass() == Explosion.class || activeForces.get(i).getClass() == HomingThrust.class) {
      forcesResolved.add(activeForces.get(i));
      numExplosions--;
    }
}

void spawnRandomTimeBomb() {
  PVector pos = new PVector (random(size/3, 2*size/3), random(size/3+50, 2*size/3+50));
  Bomb b = new TimeBomb(pos, (int)random(60,120), 0.3, random(8,20));
  b.affectedByCollisions = true;
  b.velocity = new PVector(random(-10,10),random(-10,10));
  objectsForCreation.add(b);
}

void switchToGameOver() {
  objectsForDestruction.add(cannon);
  objectsForDestruction.add(ground);
  removeCities();
  gameState = GAMEOVER;
  objectsForCreation.add(new DefenceBomb(new PVector(0,0), cannon.position.copy(), 0f, 1, 40));
  gameOverExplosion.rewind();
  gameOverExplosion.play();
}

void switchToUpgradeMenu() {
  gameState = UPGRADEMENU;
  cannon.disable();
  disableCities();
  changeSong(null);
  updateUpgradeMenu();
}

void switchToLevel() {
  gameState = LEVEL;
  levelTimer = 0;
  bullets = levelNumber*100 + 100;
  enableCities();
  cannon.enable();
  bigExplosion.rewind();
  bigExplosion.play();
  objects.add(new DefenceBomb(new PVector(0,0), new PVector(size+100,size+100), 10, 1, 1));//used to remove processing render bug where everything is out of proportion until a new object moves
  changeSong(getLevelSong(levelNumber));
}

void leaveLevel() {
  cannon.disable();
  disableCities();
  for(int i = 0; i < objects.size(); i++) {
    Entity o = objects.get(i);
    if(o.getClass() == City.class) ((City) o).resetHP();
  }
  for(int i = 0; i < objects.size(); i++) 
    if(objects.get(i).getClass() == EnemyBomb.class 
    || objects.get(i).getClass() == DefenceBomb.class
    || objects.get(i).getClass() == ClusterBomb.class
    || objects.get(i).getClass() == HomingBomb.class) 
      objectsForDestruction.add(objects.get(i));
  removeExplosions();
  changeSong(null);
  backGroundLine1 = null;
  backGroundLine2 = null;
  backGroundLine3 = null;
  backGroundLine4Left = null;
  backGroundLine4Centre = null;
  backGroundLine4Right = null;
  getLevelSong(levelNumber).pause();
  getLevelSong(levelNumber).rewind();
  bigExplosion.rewind();
  bigExplosion.play();
}

AudioPlayer getLevelSong(int level) {
  switch(level) {
   case 0:
     return timeLapse;
   case 1:
     return Monody;
   case 2:
     return Unity;
   case 3:
     return Prelude;
   case 4:
   default:
     return infinitePower;
  }
}

void changeSong(AudioPlayer s) {
 if(currentSong != null && currentSong != s) {
   currentSong.pause();
   currentSong.rewind();
 }
 currentSong = s;
}

void disableCities() {
  for(int i = 0; i < objects.size(); i++) {
    Entity o = objects.get(i);
    if(o.getClass() == City.class) o.disable();
  }
}

void enableCities() {
  for(int i = 0; i < objects.size(); i++) {
    Entity o = objects.get(i);
    if(o.getClass() == City.class) o.enable();
  }
}

void setUpCities() {
  for (int i = 1; i < 10; i ++) {
    if (i!=5)
      objectsForCreation.add(new City(size*i/10));
  }
  health = 8;
}

void removeCities() {
   for (int i=0; i < objects.size(); i++) {
    if(objects.get(i).getClass() == City.class)
      objectsForDestruction.add(objects.get(i));
  }
}

void mouseDown(PVector mouse) {
  System.out.println(levelTimer);
  switch(gameState) {
    case LOADING:
     break;
   case MAINMENU:
     if(mainMenuPlay.within(mouse)) 
       mainMenuPlayPressed = true;
     break;
   case LEVEL:
     cannon.runShootScript();
     break;
   case UPGRADEMENU:
     if(upgradePowerButton.within(mouse) && upgradePowerButton.hoverable)
       powerUpgraded = true;
     if(upgradeBombButton.within(mouse) && upgradeBombButton.hoverable)
       bombUpgraded = true;
     if(upgradeRateOfFireButton.within(mouse) && upgradeRateOfFireButton.hoverable)
       ROFUpgraded = true;
     if(upgradeReBuildCityButton.within(mouse) && upgradeReBuildCityButton.hoverable)
       citiesRebuilt = true;
     if(upgradeToClusterButton.within(mouse) && upgradeToClusterButton.hoverable)
       clusterUpgraded = true;
     if(upgradeToRailButton.within(mouse) && upgradeToRailButton.hoverable)
       railUpgraded = true;
     if(upgradeToHomingButton.within(mouse) && upgradeToHomingButton.hoverable)
       homingUpgraded = true;
     if(upgradeBeginNextLevelButton.within(mouse))
       upgradeMenuPlayPressed = true;
     break;
   case GAMEOVER:
     break;
   case PHYSICSSIMULATION:
     runPhysicsIntersect();
  }
}

void runPhysicsIntersect() {
  PVector mouse = new PVector(mouseX,mouseY);
  for(int i = 0; i < objects.size(); i++) {
    if(objects.get(i).touching(mouse)) 
      objects.get(i).velocity.add(new PVector(random(-10,10),random(-10,10)));
  }
}

//a slightly more readable way to spawn an enemy bomb
void spawnBomb(float s, int col) {
  objectsForCreation.add(new EnemyBomb(s, new PVector(random(10,size-10), -10), col));
}

void completeLevel() {
 leaveLevel();
 levelNumber++;
 switchToUpgradeMenu();
}

void runLevel0Scripts() {
  switch(levelTimer) {
   case 1:
   backGroundLine1 = "click the mouse";
   backGroundLine2 = "to shoot a missile!";
   break;
   case 375:
   backGroundLine1 = null;
   backGroundLine2 = null;
   break;
   case 475:
   backGroundLine1 = "shoot the comet";
   backGroundLine2 = "to earn gold!";
   break;
   case 700:
   backGroundLine3 = "here it comes!";
   objectsForCreation.add(new EnemyBomb(7f, new PVector(size/2, -10), RED));
   break;
   case 825:
   backGroundLine1 = null;
   backGroundLine2 = null;
   backGroundLine3 = null;
   break;
   case 1150:
   backGroundLine1 = "Knock Comets away!";
   break;
   case 1400:
   backGroundLine1 = null;
   break;
   case 1500:
   backGroundLine4Left = "Don't use up all your ammo";
   break;
   case 1800:
   backGroundLine4Right = "Don't let the comets hit cities";
   backGroundLine4Left = null;
   break;
   case 2100:
   backGroundLine4Centre = "Spend gold on upgrades";
   backGroundLine4Right = null;
   break;
   case 2400:
   backGroundLine4Centre = null;
   break;
   case 2700:
   backGroundLine1 = "Are you ready?";
   break;
   case 3000:
   backGroundLine1 = null;
   break;
   case 3175:
   backGroundLine1 = "3";
   break;
   case 3300:
   backGroundLine2 = "2";
   backGroundLine1 = null;
   break;
   case 3425:
   backGroundLine3 = "1";
   backGroundLine2 = null;
   break;
   case 3550:
   backGroundLine2 = "LEVEL 1";
   backGroundLine3 = null;
   break;
   case 3800:
   backGroundLine2 = null;
   spawnBomb(7,RED);
   spawnBomb(7,RED);
   break;
   case 3900:
   spawnBomb(7,RED);
   break;
   case 7250:
   backGroundLine2 = "Keep Going!";
   break;
   case 7500:
   backGroundLine2 = null;
   break;
   case 11100:
    backGroundLine2 = "LEVEL COMPLETE!";
   break;
   case 11700:
   completeLevel();
  }
  
  if(levelTimer > 3800 && levelTimer < 6900) 
    if(random(200) < 1) 
      spawnBomb(7,RED);
  
  if(levelTimer > 7450 && levelTimer < 10000) 
    if(random(160) < 1) 
      spawnBomb(8,RED);
    
  if(levelTimer > 10000 && levelTimer < 10800) 
   if(random(80) < 1) 
      spawnBomb(8,RED);
}

void runLevel1Scripts() {
  switch(levelTimer) {
    case 200:
    backGroundLine1 = "Some Comets behave";
    backGroundLine2 = "rather Strangely...";
    break;
    case 400:
    backGroundLine3 = "They avoid danger"; 
    break;
    case 700:
    backGroundLine1 = null;
    backGroundLine2 = null;
    backGroundLine3 = null;
    break;
    case 975:
    backGroundLine2 = "LEVEL 2";
    break;
    case 1300:
    backGroundLine2 = null;
    spawnBomb(9,BLUE);
    break;
    case 4500:
    backGroundLine1 = "Is this too";
    backGroundLine2 = "EASY?";
    break;
    case 4800:
    backGroundLine1 = null;
    backGroundLine2 = null;
    spawnBomb(12,BLUE);
    break;
    case 6000:
    backGroundLine2 = "are you ready?";
    break;
    case 6400:    
    backGroundLine2 = null;
    spawnBomb(random(7,11), r.nextInt(GREEN));
    spawnBomb(random(7,11), r.nextInt(GREEN));
    spawnBomb(random(7,11), r.nextInt(GREEN));
    spawnBomb(random(7,11), r.nextInt(GREEN));
    break;
    case 10750:
    backGroundLine2 = "LEVEL COMPLETE!";
    break;
    case 11600:
    completeLevel();
  }

  if(levelTimer >= 1300 && levelTimer <= 4000) 
    if(random(140) < 1)
    spawnBomb(9,BLUE);
 
  if(levelTimer >= 4800 && levelTimer <= 5800) 
    if(random(120) < 1)
      spawnBomb(random(7,11), BLUE);
  
  if(levelTimer >= 6400 && levelTimer <= 9500) 
    if(random(90) < 1)
      spawnBomb(random(7,11), r.nextInt(GREEN));
      
  if(levelTimer >= 9500 && levelTimer <= 10200) 
    if(random(50) < 1)
      spawnBomb(random(7,11), r.nextInt(GREEN));
}


void runLevel2Scripts() {
  switch(levelTimer) {
    case 1:
    backGroundLine1 = "green comets home";
    backGroundLine2 = "in on their targets";
    break;
    case 300:
    backGroundLine3 = "but are worth more!";
    break;
    case 700:
    backGroundLine1 = null;
    backGroundLine2 = null;
    backGroundLine3 = null;
    break;
    case 840:
    backGroundLine2 = "LEVEL 3";
    break;
    case 1100:
    backGroundLine2 = null;
    spawnBomb(11, GREEN);
    break;
    case 5100:
    backGroundLine2 = "are you ready?";
    break;
    case 5400:
    backGroundLine2 = null; 
    break;
    case 5500:
    spawnBomb(random(10,13), r.nextInt(PINK));
    spawnBomb(random(10,13), r.nextInt(PINK));
    break;
    case 9900:
    backGroundLine2 = "LEVEL COMPLETE!";
    break;
    case 10600:
    completeLevel();
    break;
  }
  
  if(levelTimer >= 1100 && levelTimer <= 3000) 
    if(random(160) < 1) 
      spawnBomb(random(8,13), GREEN);
  
  if(levelTimer >= 3000 && levelTimer <= 4800) 
    if(random(120) < 1) 
      spawnBomb(random(8,13), GREEN);
    
  if(levelTimer >= 5500 && levelTimer <= 8700) 
    if(random(90) < 1) 
      spawnBomb(random(10,13), r.nextInt(PINK));
    
  if(levelTimer >= 8700 && levelTimer <= 9400) 
    if(random(40) < 1) 
      spawnBomb(random(5,10), GREEN);
}

void runLevel3Scripts() {
  switch(levelTimer) {
    case 1:
    backGroundLine1 = "this is the";
    backGroundLine2 = "last tutorial!";
    break;
    case 200:
    backGroundLine1 = null;
    backGroundLine2 = null;
    break;
    case 300:
    backGroundLine2 = "pink comets";
    backGroundLine3 = "are more dense...";
    break;
    case 600:
    backGroundLine2 = null;
    backGroundLine3 = null;
    break;
    case 750:
    backGroundLine2 = "LEVEL 4";
    break;
    case 1000:
    backGroundLine2 = null;
    spawnBomb(14, PINK);
    break;
    case 6400:
    backGroundLine2 = "are you ready?";
    break;
    case 6650:
    backGroundLine2 = null;
    break;
    case 6900:
    spawnBomb(13, RED);
    spawnBomb(13, GREEN);
    spawnBomb(13, BLUE);
    spawnBomb(13, PINK);
    break;
    case 13000:
    backGroundLine2 = "LEVEL COMPLETE!";
    break;
    case 13600:
    completeLevel();
  }
  
  if(levelTimer >= 1000 && levelTimer <= 3000) 
    if(random(300) < 1)
      spawnBomb(random(10,15), PINK);
     
  if(levelTimer >= 1000 && levelTimer <= 4000) 
    if(random(300) < 1)
      spawnBomb(random(10,15), PINK);
      
  if(levelTimer >=3000 && levelTimer <= 5000) 
    if(random(100) < 1)
      spawnBomb(random(10,15), r.nextInt(PINK));
      
  if(levelTimer >=5000 && levelTimer <= 6000) 
    if(random(40) < 1)
      spawnBomb(random(6,10), r.nextInt(PINK));
  
  if(levelTimer >= 6900 && levelTimer <= 12000) 
    if(random(30) < 1)
      spawnBomb(random(7,9), r.nextInt(PINK));
  
  if(levelTimer >=6900 && levelTimer <= 12000 && levelTimer%200 == 0) 
      spawnBomb(12, PINK);
      
  if(levelTimer >= 12000 && levelTimer <= 12500) 
    if(random(20) < 1)
      spawnBomb(random(7,10), r.nextInt(PINK+1));
}


void runLevel4Scripts() {
  switch(levelTimer) {
    case 1:
    backGroundLine2 = "LEVEL 5";
    break;
    case 300:
    backGroundLine2 = null;
    break;
    case 600:
    spawnBomb(random(8,12), r.nextInt(PINK+1));
    spawnBomb(random(8,12), r.nextInt(PINK+1));
    spawnBomb(random(8,12), r.nextInt(PINK+1));
    break;
    case 750:
    case 900:
    case 1050:
    spawnBomb(random(8,12), r.nextInt(PINK+1));
    break;
    case 1200:
    spawnBomb(random(8,12), r.nextInt(PINK+1));
    spawnBomb(random(8,12), r.nextInt(PINK+1));
    spawnBomb(random(8,12), r.nextInt(PINK+1));
    break;
    case 4100:
    backGroundLine2 = "are you ready?";
    break;
    case 4250:
    backGroundLine2 = null;
    break;
    case 4402:
    backGroundLine1 = "3";
    break;
    case 4472:
    backGroundLine1 = null;
    backGroundLine2 = "2";
    break;
    case 4542:
    backGroundLine2 = null;
    backGroundLine3 = "1";
    break;
    case 4612:
    backGroundLine3 = null;
    break;
    case 4682:
    spawnBomb(10, RED);
    spawnBomb(10, RED);
    spawnBomb(10, RED);
    spawnBomb(10, RED);
    break;
    case 24550:
    changeSong(null);
    break;
    case 24850:
    backGroundLine2 = "GAME COMPLETE!";
    break;
    case 25400:
    switchToGameOver();
  }
   if(levelTimer >= 1200 && levelTimer <= 2000) 
    if(random(60) < 1)
      spawnBomb(random(8,12), r.nextInt(PINK+1));
      
   if(levelTimer >= 2400 && levelTimer <= 3600) 
    if(random(60) < 1)
      spawnBomb(random(12,14), r.nextInt(PINK+1));
      
   if(levelTimer >= 3600 && levelTimer <= 4200) 
    if(random(60) < 1)
      spawnBomb(random(14,16), r.nextInt(PINK+1));
      
   if(levelTimer >= 4682 && levelTimer <= 6000) 
    if(random(14) < 1)
      spawnBomb(10, RED);
      
   if(levelTimer >= 6000 && levelTimer <= 12000) 
    if(random(14) < 1)
      spawnBomb(random(8,10), r.nextInt(PINK+1));
      
   if(levelTimer >= 12000 && levelTimer <= 16000) 
    if(random(40) < 1)
      spawnBomb(random(12,16), r.nextInt(PINK+1));
      
   if(levelTimer >= 12000 && levelTimer <= 16000) 
    if(random(40) < 1)
      spawnBomb(10, GREEN);
      
   if(levelTimer >= 16000 && levelTimer <= 19000) 
    if(random(16) < 1)
      spawnBomb(12, BLUE);
      
   if(levelTimer >= 19000 && levelTimer <= 23000) 
    if(random(14) < 1)
      spawnBomb(10, r.nextInt(PINK+1));
   
   if(levelTimer >= 23000 && levelTimer <= 24000) 
    if(random(10) < 1)
      spawnBomb(random(5,14), RED);
   
}