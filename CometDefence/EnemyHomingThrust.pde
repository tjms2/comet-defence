class EnemyHomingThrust extends HomingThrust {
  
  
  EnemyHomingThrust(Entity s, float p, float r) {
   super(s,p,r);
  }
  
  boolean isEngagingForce(Entity o) {
    return ((o.getClass() == DefenceBomb.class ||
          o.getClass() == ClusterBomb.class || 
          o.getClass() == HomingBomb.class || 
          o.getClass() == TimeBomb.class ) &&
          ((Bomb) o).renderState == ((Bomb) o).NORMALBOMB);
  }
  
}