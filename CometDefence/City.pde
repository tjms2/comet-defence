class City extends Entity {
  
  int damageLevel;
  int cityLevel;
  
  City(float xVal) {
    velocity = new PVector(0,0);
    forces = new PVector(0,0);
    radius = 30f;
    position = new PVector(xVal, size+radius/2);   
    cityLevel = 3;
  }
  
  void render(){
    if(enabled) {
      fill(255*(cityLevel-damageLevel)/cityLevel);
      ellipse(position.x, position.y, radius, radius);
    }
  }
  
  void runScripts(){
    //nothing yet!
  }
  
  void damage() {
    damageLevel++;
    if(damageLevel==cityLevel) {
      health--;
      objectsForDestruction.add(this); 
    }
  }
  
  void resetHP() { 
   damageLevel = 0; 
  }
}