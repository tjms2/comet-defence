class Ground extends Entity {

  
  Ground() {
   radius = 999999f;
   mass = PI*radius*radius;
   position = new PVector(0, size+radius);
   velocity = new PVector(0,0);
   affectedByCollisions = true;
   affectedByDrag = true;
   enabled = true;
  }
  
  void render() {}
  
  void runScripts() {
   position = new PVector(0, size+radius);
  }
 
}