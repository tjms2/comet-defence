class TimeBomb extends Bomb {
  int lifeSpan;
  
  
   
  TimeBomb(PVector position, int lifeExpectancy, float bombLevel, float r) {
    lifeSpan = lifeExpectancy;
    affectedByGravity = true;
    affectedByDrag = true;
    affectedByExplosions = true;
    this.position = position;
    velocity = new PVector(0,0);
    radius = r;
    mass = PI*radius;
    renderState = NORMALBOMB;
    this.bombLevel = bombLevel;
    enabled = true;
  }
  
  boolean detonate() {
    return lifeSpan <= 0;
  }
  void renderNormal() {
    renderVanillaBomb();
  }

  void runScripts() {
    lifeSpan --;
    runBombExplosionScript();
  }
}