
MenuItem loadingMenu;

MenuBox mainMenu;
MenuBox mainMenuPlay;

MenuBox upgradeMenu;
MenuText upgradeHeader;
MenuText upgradeHeaderInner;
MenuBox upgradePowerButton;
MenuText upgradePowerText;
MenuText upgradePowerCost;
MenuBox upgradeBombButton;
MenuText upgradeBombText;
MenuText upgradeBombCost;
MenuBox upgradeRateOfFireButton;
MenuText upgradeRateOfFireText;
MenuText upgradeRateOfFireCost;
MenuBox upgradeReBuildCityButton;
MenuText upgradeReBuildText;
MenuBox upgradeToClusterButton;
MenuText upgradeToClusterText;
MenuText upgradeToClusterCost;
MenuBox upgradeToHomingButton;
MenuText upgradeToHomingText;
MenuText upgradeToHomingCost;
MenuBox upgradeToRailButton;
MenuText upgradeToRailText;
MenuText upgradeToRailCost;
MenuBox upgradeBeginNextLevelButton;

MenuBox hud;
MenuText hudAmmo;
MenuText hudGold;
MenuText hudCities;


int numExplosions;
int maxLoad = 300;
String backGroundLine1;
String backGroundLine2;
String backGroundLine3;
String backGroundLine4Left;
String backGroundLine4Centre;
String backGroundLine4Right;



void setUpHUD() {
  textFont(createFont("Futuristic.ttf", 64));
  
  loadingMenu = new MenuText(new PVector(size/2, (size+100)/2), "LOADING...", 200, 0, 0, CENTER, 64);
  
  mainMenu = new MenuBox(new PVector(size/2, (size+100)/2), size/3, size/3, 100,100,100);
  MenuBox mainMenuInner = new MenuBox(new PVector(size/2, (size+100)/2), size/3-10, size/3-10, 120,120,120);
  mainMenu.children.add(mainMenuInner);
  mainMenuPlay = new MenuBox(new PVector(size/2, (size+100)/2+50), size/6, size/8, 100,100,100);
  mainMenuInner.children.add(mainMenuPlay);
  mainMenuPlay.hoverable = true;
  mainMenuPlay.children.add(new MenuText(new PVector(size/2, (size+100)/2+45), "BEGIN", 200, 0, 0, CENTER, 62));
  mainMenuPlay.children.add(new MenuText(new PVector(size/2, (size+100)/2+45), "BEGIN", 0, 0, 0, CENTER, 64));
  mainMenuInner.children.add(new MenuText(new PVector(size/2, ((size+100)/2)-size/4), "COMET", 200, 0, 0, CENTER, 62));
  mainMenuInner.children.add(new MenuText(new PVector(size/2, ((size+100)/2)-size/4), "COMET", 0, 0, 0, CENTER, 64));
  mainMenuInner.children.add(new MenuText(new PVector(size/2, ((size+100)/2)-size/6), "DEFENCE", 200, 0, 0, CENTER, 62));
  mainMenuInner.children.add(new MenuText(new PVector(size/2, ((size+100)/2)-size/6), "DEFENCE", 0, 0, 0, CENTER, 64));
  
  mainMenuInner.children.add(new MenuText(new PVector(size/2, ((size+100)/2)+size/4), "music courtesy of TheFatRat", 0, 0, 0, CENTER, 24));
  
  
  
  upgradeMenu = new MenuBox(new PVector(size/2, size/2), size*0.4, size*0.4, 120,120,120);
  MenuBox upgradeMenuInner = new MenuBox(new PVector(size/2, size/2), size*0.4-10, size*0.4-10, 150,150,150);
  upgradeMenu.children.add(upgradeMenuInner);
  upgradeHeader = new MenuText(new PVector(size/2, size*0.15+10), "UPGRADES", 240,0,0, CENTER, 64);
  upgradeHeaderInner = new MenuText(new PVector(size/2, size*0.15+10), "UPGRADES", 10,10,10, CENTER, 62);
  upgradeMenuInner.children.add(upgradeHeader);
  upgradeMenuInner.children.add(upgradeHeaderInner);
  
  upgradePowerButton = makeUpgradeMenuButton(new PVector(size/3,size*0.3));
  upgradePowerText = new MenuText(new PVector(size/3,size*0.3-15), "POWER ", 0,0,0, CENTER, 24, 1);
  upgradePowerCost = new MenuText(new PVector(size/3,size*0.3+15), "cost: ", 0,0,0, CENTER, 24, 100);
  upgradePowerText.roman = true;
  upgradePowerButton.children.add(upgradePowerText);
  upgradePowerButton.children.add(upgradePowerCost);
  
  upgradeBombButton = makeUpgradeMenuButton(new PVector(size/3,size*0.45));
  upgradeBombText = new MenuText(new PVector(size/3,size*0.45-15), "MISSILES ", 0,0,0, CENTER, 24, 1);
  upgradeBombCost = new MenuText(new PVector(size/3,size*0.45+15), "cost: ", 0,0,0, CENTER, 24, 100);
  upgradeBombText.roman = true;
  upgradeBombButton.children.add(upgradeBombText);
  upgradeBombButton.children.add(upgradeBombCost);
  
  upgradeRateOfFireButton = makeUpgradeMenuButton(new PVector(size/3,size*0.6));
  upgradeRateOfFireText = new MenuText(new PVector(size/3,size*0.6-15), "FIRE RATE ", 0,0,0, CENTER, 24, 1);
  upgradeRateOfFireCost = new MenuText(new PVector(size/3,size*0.6+15), "cost: ", 0,0,0, CENTER, 24, 100);
  upgradeRateOfFireText.roman = true;
  upgradeRateOfFireButton.children.add(upgradeRateOfFireText);
  upgradeRateOfFireButton.children.add(upgradeRateOfFireCost);
  
  upgradeReBuildCityButton = makeUpgradeMenuButton(new PVector(size/3,size*0.75));
  upgradeReBuildText = new MenuText(new PVector(size/3,size*0.75+15), "cost: ", 0,0,0, CENTER, 24, 250);
  upgradeReBuildCityButton.children.add(new MenuText(new PVector(size/3,size*0.75-15), "REBUILD CITIES", 0,0,0, CENTER, 24));
  upgradeReBuildCityButton.children.add(upgradeReBuildText);
  
  upgradeToClusterButton = makeUpgradeMenuButton(new PVector(size*(2f/3f),size*0.3));
  upgradeToClusterText = new MenuText(new PVector(size*(2f/3f),size*0.3-15), "CLUSTER CANNON", 0,0,0, CENTER, 20);
  upgradeToClusterCost = new MenuText(new PVector(size*(2f/3f),size*0.3+15), "cost: ",0,0,0,CENTER, 24, 1000);
  upgradeToClusterButton.children.add(upgradeToClusterText);
  upgradeToClusterButton.children.add(upgradeToClusterCost);
  
  upgradeToHomingButton = makeUpgradeMenuButton(new PVector(size*(2f/3f),size*0.45));
  upgradeToHomingText = new MenuText(new PVector(size*(2f/3f),size*0.45-15), "HOMING CANNON", 0,0,0, CENTER, 20);
  upgradeToHomingCost = new MenuText(new PVector(size*(2f/3f),size*0.45+15), "cost: ", 0,0,0, CENTER, 20, 500);
  upgradeToHomingButton.children.add(upgradeToHomingText);
  upgradeToHomingButton.children.add(upgradeToHomingCost);
  
  upgradeToRailButton = makeUpgradeMenuButton(new PVector(size*(2f/3f),size*0.6));
  upgradeToRailText = new MenuText(new PVector(size*(2f/3f),size*0.6-15), "RAIL CANNON", 0,0,0, CENTER, 20);
  upgradeToRailCost = new MenuText(new PVector(size*(2f/3f),size*0.6+15), "cost: ", 0,0,0, CENTER, 20, 200);
  upgradeToRailButton.children.add(upgradeToRailText);
  upgradeToRailButton.children.add(upgradeToRailCost);  
  
  upgradeBeginNextLevelButton = makeUpgradeMenuButton(new PVector(size*(2f/3f),size*0.75));
  upgradeBeginNextLevelButton.children.add(new MenuText(new PVector(size*(2f/3f),size*0.75), "BEGIN", 200,0,0, CENTER, 34));
  upgradeBeginNextLevelButton.children.add(new MenuText(new PVector(size*(2f/3f),size*0.75), "BEGIN", 0,0,0, CENTER, 36));
  
  upgradeMenuInner.children.add(upgradePowerButton);
  upgradeMenuInner.children.add(upgradeBombButton);
  upgradeMenuInner.children.add(upgradeRateOfFireButton);
  upgradeMenuInner.children.add(upgradeReBuildCityButton);
  upgradeMenuInner.children.add(upgradeToClusterButton);
  upgradeMenuInner.children.add(upgradeToHomingButton);
  upgradeMenuInner.children.add(upgradeToRailButton);
  upgradeMenuInner.children.add(upgradeBeginNextLevelButton);
  
  hud = new MenuBox(new PVector(size/2, size+50), size/2, 50, 120,120,120);
  MenuBox hudInner = new MenuBox(new PVector(size/2, size+50), size/2-10, 40, 150,150,150);
  hud.children.add(hudInner);
  hudAmmo = new MenuText(new PVector(30,size+50), "AMMO: ", 140,0,0,LEFT,32, bullets);
  hudGold = new MenuText(new PVector(size/2,size+50), "GOLD: ", 140,0,0,CENTER,32, gold);
  hudCities = new MenuText(new PVector(size-30,size+50), "CITIES: ", 140,0,0,RIGHT,32, health);
  hud.children.add(hudAmmo);
  hud.children.add(hudGold);
  hud.children.add(hudCities);
}

  
MenuBox makeUpgradeMenuButton(PVector pos) {
  MenuBox x = new MenuBox(pos, size/8, size/16 , 100,100,100);
  x.hoverable = true;
  return x;
}

void renderHUD() {
  updateHUD();
  hud.render();
}

void updateHUD() { 
  hudAmmo.numericalContents = bullets;
  hudGold.numericalContents = gold;
  hudCities.numericalContents = health;
}


void updateUpgradeMenu() {
  upgradePowerText.numericalContents = powerUpgrade;
  upgradePowerCost.numericalContents = powerUpgrade * 100;
  upgradePowerButton.hoverable = (upgradePowerCost.numericalContents <= gold) ? true : false;
  
  upgradeBombText.numericalContents = bombUpgrade;
  upgradeBombCost.numericalContents = bombUpgrade * 100;
  upgradeBombButton.hoverable = (upgradeBombCost.numericalContents <= gold) ? true : false;
  
  upgradeRateOfFireText.numericalContents = ROFUpgrade;
  upgradeRateOfFireCost.numericalContents = ROFUpgrade * 100;
  upgradeRateOfFireButton.hoverable = (upgradeRateOfFireCost.numericalContents <= gold) ? true : false;
  
  upgradeReBuildCityButton.hoverable = (250 <= gold) ? true : false;
  
  switch(cannon.cannonType) {
    case VANILLACANNON:
      upgradeToClusterButton.hoverable = true;
      upgradeToHomingButton.hoverable = true;
      upgradeToRailButton.hoverable = true;
      upgradeToClusterText.contents = "CLUSTER CANNON";
      upgradeToClusterCost.contents = "cost: ";
      upgradeToClusterCost.numericalContentsDefined = true;
      upgradeToClusterCost.numericalContents = 1000;
      upgradeToHomingText.contents = "HOMING CANNON";
      upgradeToHomingCost.contents = "cost: ";
      upgradeToHomingCost.numericalContentsDefined = true;
      upgradeToHomingCost.numericalContents = 500;
      upgradeToRailText.contents = "RAIL CANNON";
      upgradeToRailCost.contents = "cost: ";
      upgradeToRailCost.numericalContentsDefined = true;
      upgradeToRailCost.numericalContents = 250;
      upgradeToClusterButton.hoverable = (upgradeToClusterCost.numericalContents <= gold) ? true : false;
      upgradeToHomingButton.hoverable = (upgradeToHomingCost.numericalContents <= gold) ? true : false;
      upgradeToRailButton.hoverable = (upgradeToRailCost.numericalContents <= gold) ? true : false;
      break;
    case CLUSTERCANNON:
      upgradeToClusterText.contents = "CLUSTER CANNON ";
      upgradeToClusterCost.contents = "cost: ";
      upgradeToClusterCost.numericalContentsDefined = true;
      upgradeToClusterText.numericalContentsDefined = true;
      upgradeToClusterText.roman = true;
      upgradeToClusterCost.numericalContents = clusterCannonUpgrade*200;
      upgradeToClusterText.numericalContents = clusterCannonUpgrade;
      upgradeToHomingButton.hoverable = false;
      upgradeToHomingText.contents = "N/A";
      upgradeToHomingCost.contents = "not compatible";
      upgradeToHomingText.numericalContentsDefined = false;
      upgradeToHomingCost.numericalContentsDefined = false;
      upgradeToRailButton.hoverable = false;
      upgradeToRailText.contents = "N/A";
      upgradeToRailCost.contents = "not compatible";
      upgradeToRailCost.numericalContentsDefined = false;
      upgradeToRailText.numericalContentsDefined = false;
      upgradeToClusterButton.hoverable = (upgradeToClusterCost.numericalContents <= gold) ? true : false;
      break;
    case RAILCANNON:
      upgradeToRailText.contents = "RAIL CANNON ";
      upgradeToRailCost.contents = "cost: ";
      upgradeToRailCost.numericalContentsDefined = true;
      upgradeToRailText.numericalContentsDefined = true;
      upgradeToRailText.roman = true;
      upgradeToRailCost.numericalContents = railCannonUpgrade*100;
      upgradeToRailText.numericalContents = railCannonUpgrade;
      upgradeToHomingButton.hoverable = false;
      upgradeToHomingText.contents = "N/A";
      upgradeToHomingCost.contents = "not compatible";
      upgradeToHomingText.numericalContentsDefined = false;
      upgradeToHomingCost.numericalContentsDefined = false;
      upgradeToClusterButton.hoverable = false;
      upgradeToClusterText.contents = "N/A";
      upgradeToClusterCost.contents = "not compatible";
      upgradeToClusterCost.numericalContentsDefined = false;
      upgradeToClusterText.numericalContentsDefined = false;
      upgradePowerButton.hoverable = false;
      upgradePowerText.contents = "N/A";
      upgradePowerText.numericalContentsDefined = false;
      upgradePowerCost.contents = "not compatible";
      upgradePowerCost.numericalContentsDefined = false;
      upgradeToRailButton.hoverable = (upgradeToRailCost.numericalContents <= gold) ? true : false;
      break;
    case HOMINGCANNON:
      upgradeToHomingText.contents = "HOMING CANNON ";
      upgradeToHomingCost.contents = "cost: ";
      upgradeToHomingCost.numericalContentsDefined = true;
      upgradeToHomingText.numericalContentsDefined = true;
      upgradeToHomingText.roman = true;
      upgradeToHomingCost.numericalContents = homingCannonUpgrade*200;
      upgradeToHomingText.numericalContents = homingCannonUpgrade;
      upgradeToRailButton.hoverable = false;
      upgradeToRailText.contents = "N/A";
      upgradeToRailCost.contents = "not compatible";
      upgradeToRailText.numericalContentsDefined = false;
      upgradeToRailCost.numericalContentsDefined = false;
      upgradeToClusterButton.hoverable = false;
      upgradeToClusterText.contents = "N/A";
      upgradeToClusterCost.contents = "not compatible";
      upgradeToClusterCost.numericalContentsDefined = false;
      upgradeToClusterText.numericalContentsDefined = false;
      upgradeToHomingButton.hoverable = (upgradeToHomingCost.numericalContents <= gold) ? true : false;
  }
  
  if(levelNumber == 4) {
    upgradeHeader.contents = "FINAL UPGRADES!";
    upgradeHeaderInner.contents = "FINAL UPGRADES!";
    upgradeHeaderInner.textSize = 63;
  }
}

void renderLoadingScreen() {
  if(loadingTime<maxLoad-95)
  loadingMenu.render();//hacky way to get the drop effect
}

void renderUpgradeMenu() {
  upgradeMenu.render(); 
}

void renderMainMenu() {
  mainMenu.render();
}

void renderMainMenuBackground() {
  stroke(50);
  fill(150);
  rectMode(RADIUS);
  rect(size/2, (size+100)/2, size/3, size/3, 10, 10 , 10, 10);
  fill(200);
  rectMode(RADIUS);
  rect(size/2, (size+100)/2, size/3-10, (size)/3 - 10, 10, 10 , 10, 10);
}

void renderBackground() {
  if(levelNumber == 4 && levelTimer >= 4405 && levelTimer <=4600) {
    background((levelTimer - 4405));
  } else {
    background(10+numExplosions*10, 0, 10);
  }
  fill(140,0,0);
  if(backGroundLine1!=null) {
    textSize(64);
    textAlign(CENTER);
    text(backGroundLine1, size/2, size*0.2);
  }
  if(backGroundLine2!=null) {
    textSize(64);
    textAlign(CENTER);
    text(backGroundLine2, size/2, size*0.4);
    
  }
  if(backGroundLine3!=null) {
    textSize(64);
    textAlign(CENTER);
    text(backGroundLine3, size/2, size*0.6);
    
  }
  if(backGroundLine4Left!=null) {
    textSize(32);
    textAlign(LEFT);
    text(backGroundLine4Left, size*0.05, size*0.9);
    
  }if(backGroundLine4Centre!=null) {
    textSize(32);
    textAlign(CENTER);
    text(backGroundLine4Centre, size/2, size*0.9);
    
  }if(backGroundLine4Right!=null) {
    textSize(32);
    textAlign(RIGHT);
    text(backGroundLine4Right, size*0.95, size*0.9);
  }
}