public class Collision {
 Entity o1;
 Entity o2;
 float coefficientOfRest;
 
 Collision(Entity obj1, Entity obj2, float c) {
   o1 = obj1;
   o2 = obj2;
   coefficientOfRest = c;
 }
 
 void resolve() {
   
   PVector relativeVel = o1.velocity.copy().sub(o2.velocity);
   PVector relativePos = o1.position.copy().sub(o2.position); 
   float postCollisionVel = relativeVel.dot(relativePos.normalize());
   float actualPostColVel = (-coefficientOfRest)*postCollisionVel;//reduced a little by the co of rest. so objects won't move away from each other quite as fast as they approached (will instead approach equal true velocities)
   float changeInPostColVel = actualPostColVel - postCollisionVel;
   float totalInvMass = o1.inverseMass()  + o2.inverseMass();
   float impulse = changeInPostColVel / totalInvMass;
   PVector impulsePerInvMass = relativePos.normalize().mult(impulse);
   PVector o1Impulse = impulsePerInvMass.copy().mult(o1.inverseMass());
   PVector o2Impulse = impulsePerInvMass.copy().mult(-o2.inverseMass());//negate this as the relativePosition of o2 to o1 is negative
  
   o1.velocity.add(o1Impulse);
   o2.velocity.add(o2Impulse);
 }
 
 boolean is(Entity obj1, Entity obj2) {
   return ((obj1 == o1 && obj2 == o2) || (obj1 == o2 && obj2 == o1));
 }
}