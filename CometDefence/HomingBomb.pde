class HomingBomb extends DefenceBomb {
  
  HomingThrust homing;
  
   HomingBomb(PVector thrust, PVector position, float distanceToMouse, float bombLevel, float r, float homingPower) {
    super(thrust, position, distanceToMouse, bombLevel, r);
    homing = new DefenceHomingThrust(this, homingPower, radius*20);
    activeForces.add(homing);
  }
  
  boolean detonate() {
    return touchingAnyBombs(radius*2) ||  touching(ground);
  }

  void runScripts() {
     if(detonate()&&renderState==NORMALBOMB) {
      exploded = true;
      forcesResolved.add(homing);
    }
    super.runScripts();
  }
  
  
  
boolean touchingAnyBombs(float r) {
  for (int i = 0; i < objects.size(); i++) {
    if (touching(objects.get(i),r) && objects.get(i).getClass() == EnemyBomb.class) return true;
  }
  return false;
}
}