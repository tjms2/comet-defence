class MenuBox extends MenuItem {
 
  //required
  float Width;
  float Height;
  
  boolean hoverable;
  
  
  public ArrayList<MenuItem> children = new ArrayList<MenuItem>();
  
  MenuBox(PVector pos, float w, float h) {
   super(pos);
   Width = w;
   Height = h;
  }
  
  MenuBox(PVector pos, float w, float h, int r, int g, int bl) {
   super(pos, r, g, bl);
   Width = w;
   Height = h;
  }
  
  void render() {
    stroke(50);
    fill(Red(),Green(),Blue());
    if(hoverable && mouseIsWithin())  {
      fill(140,0,0);
    }
    rectMode(RADIUS);
    rect(centrePos.x, centrePos.y, Width, Height, 10f);
    for(int i = 0; i < children.size(); i++) {
      children.get(i).render(); 
    }
  }
  
  boolean within(PVector pos) {
    float cX = centrePos.x;
    float cY = centrePos.y;
    return (abs(cX-pos.x) < Width) && (abs(cY-pos.y) < Height);
  }
  
  boolean mouseIsWithin() {
    return within(new PVector(mouseX,mouseY));
  }
  
  
}