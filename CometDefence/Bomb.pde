abstract class Bomb extends Entity {
  
  final int NORMALBOMB = 0;
  final int EXPLOSION1 = 1;
  final int EXPLOSION2 = 2;
  final int EXPLOSION3 = 3;
  final int EXPLOSION4 = 4;
  
  int renderState;
  int explodedTimer;
  Explosion E;
  float bombLevel;
  boolean exploded;
  boolean hasSound;
  AudioPlayer sound;//should be given if hasSound is true 
  
  abstract boolean detonate();
  abstract void renderNormal();
  
  void render() {
   stroke(50);
   if(enabled)
   switch(renderState) {
    case NORMALBOMB:
    renderNormal();
    break;
    case EXPLOSION1:
    renderExplosion1();
    break;
    case EXPLOSION2:
    renderExplosion2();
    break;
    case EXPLOSION3:
    renderExplosion3();
    break;
    case EXPLOSION4:
    renderExplosion4();
    break;
   }
  }
  
  void runBombExplosionScript() {
    if(renderState==NORMALBOMB) 
      if(detonate() || exploded) {
        renderState = EXPLOSION1;
        E = new Explosion(this, bombLevel);
        activeForces.add(E);
        affectedByCollisions = false;
        affectedByGravity = false;
        affectedByExplosions = false;
        explodedTimer = 0;
        velocity.mult(0);
        numExplosions++;
        if(hasSound) {
          makeExplosionSound();
        }
      } 
    if(renderState!=NORMALBOMB) {
       switch(explodedTimer) {
        case 10:
        forcesResolved.add(E);
        numExplosions --;
        renderState=EXPLOSION2;
        break;
        case 20:
        renderState=EXPLOSION3;
        break;
        case 30:
        renderState=EXPLOSION4;
        break;
        case 40:
        objectsForDestruction.add(this);
      }
      explodedTimer++;
    }
  }
  
  
  void renderExplosion1() {
    fill(255,255,0);
    ellipse(position.x, position.y, radius*2.4,radius*2.4);
    fill(255,255,255);
    ellipse(position.x, position.y, radius*2,radius*2);
  }
  void renderExplosion2() {
    fill(255,0,0);
    ellipse(position.x, position.y, radius*4,radius*4);
    fill(255,128,0);
    ellipse(position.x, position.y, radius*3,radius*3);
    fill(255,255,0);
    ellipse(position.x, position.y, radius*2,radius*2);
  }
  void renderExplosion3() {
    fill(60);
    ellipse(position.x, position.y, radius*6,radius*6);
    fill(220,100,50);
    ellipse(position.x, position.y, radius*4,radius*4);
    fill(255,158,0);
    ellipse(position.x, position.y, radius*3,radius*3);
  }
  void renderExplosion4() {
    fill(80);
    ellipse(position.x, position.y, radius*3,radius*3);    
  }
  
  void renderVanillaBomb() {
    stroke(50);
    ellipseMode(RADIUS);
    fill(200);
    ellipse(position.x, position.y, radius,radius);
    fill(150);
    stroke(20);
    ellipse(position.x, position.y, radius*0.9,radius*0.9);
    fill(250);
    stroke(100);
    ellipse(position.x-radius/4, position.y-radius/4, radius/2, radius/2);
  }
  
  void makeExplosionSound() {
    sound.rewind();
    sound.play();
  }
}