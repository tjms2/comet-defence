class MenuText extends MenuItem {
 
  String  contents;
  int numericalContents;//for things like "ammo: 10"
  boolean numericalContentsDefined;
  boolean roman;
  //optional 
  int textSize;
  int layout;
  boolean textTypeDefined;
  
  
  MenuText(PVector pos, String str) {
    super(pos);
    contents = str;
  }
  
  MenuText(PVector pos, String str, int r, int g, int b, int layout, int size) {
    super(pos, r, g, b);
    contents = str;
    textTypeDefined = true;
    textSize = size;
    this.layout = layout;
  }
  
  MenuText(PVector pos, String str, int r, int g, int b, int layout, int size, int numerical) {
    super(pos, r, g, b);
    contents = str;
    textTypeDefined = true;
    textSize = size;
    this.layout = layout;
    numericalContentsDefined = true;
    numericalContents = numerical;
  }
  
  void render() {
    textAlign(Layout(), CENTER);
    textSize(Size());
    stroke(50);
    fill(Red(),Green(),Blue());
    text(getContents(), centrePos.x, centrePos.y);
  }
  
  String getContents() {
   if(numericalContentsDefined) {
     return contents + (roman ? toRomanNumericals(numericalContents) : Integer.toString(numericalContents));
   }
   return contents;
  }
  
  int Size() {
   return textTypeDefined ? textSize : 32;
  }
  
  int Layout() {
   return textTypeDefined ? layout : CENTER;
  }
  
  String toRomanNumericals(int i) {
    switch(i) {
    case 1:
      return "I";
    case 2:
      return "II";
    case 3:
      return "III";
    case 4:
      return "IV";
    case 5:
      return "V";
    case 6:
      return "VI";
    case 7:
      return "VII";
    case 8:
      return "VIII";
    case 9:
      return "IX";
    case 10:
      return "X";
    default:
    return Integer.toString(i);
    }
  }
  
}