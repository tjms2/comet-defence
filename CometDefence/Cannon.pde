class Cannon extends Entity {
  
  
  boolean recoiled;
  int cannonType;
  float recoilTimer;
  float powerLevel;
  float rateOfFireLevel;
  float missileSize;
  float missileLevel;
  
  int clusterCannonLevel =4;
  float homingCannonLevel = 0.05;
  
  PVector mouse;
  
  Cannon() {
    position = new PVector(size/2, size);
    velocity = new PVector(0,0);
    forces = new PVector(0,0);
    radius = 30f;
    mass = radius*PI*radius;
    powerLevel = 125;
    rateOfFireLevel = 2;
    missileSize = 6.5;
    missileLevel = 3;
    clusterCannonLevel =4;
    homingCannonLevel = 0.05;
    mouse = new PVector(size/2, 0);
    cannonType = VANILLACANNON;
  }
  
  void render(){
    if(enabled) {
      noStroke();
      for(int i = 1; i < 11; i++) {
        PVector relativePosAngle = position.copy().sub(mouse).normalize().mult((radius+recoilQuant())*i/10f);
        fill(120);
        ellipse(position.x-relativePosAngle.x, position.y-relativePosAngle.y, radius/4, radius/4);
       
      }
      stroke(20);
      fill(200,0,0);
      ellipse(position.x, position.y, radius, radius);
      fill(60);
      ellipse(position.x, position.y, radius*0.8, radius*0.8);
    }
  }
  
  float recoilQuant() {
      return radius/2f - ((radius/2f)*(1f/30f)*recoilTimer);
  }
  
  void runScripts() {
    if(enabled) mouse = new PVector(mouseX, mouseY);
    if(recoiled) {
       recoilTimer--;
       if(recoilTimer<=0) {
         recoiled = false;
       }  
    }
  }
  
  void runShootScript(){
    if(enabled)
      if(bullets>0 && !recoiled) {
        recoiled = true;
        recoilTimer = 120/rateOfFireLevel;
        bullets--;
        bulletsFired++;
        shootMissile();
      }
  }
  
  void shootMissile() {
     mouse = new PVector(mouseX, mouseY);
     PVector positionShotFrom = position.copy().sub(position.copy().sub(mouse).normalize().mult((radius*2)));
     PVector thrust = position.copy().sub(mouse).normalize().mult(-powerLevel);
     Bomb b;
     switch(cannonType) {
       case VANILLACANNON:
         b = new DefenceBomb(thrust, positionShotFrom, positionShotFrom.copy().sub(mouse).mag(), missileLevel, missileSize);
         b.sound = smallExplosion1;
         break;
       case CLUSTERCANNON:
         b = new ClusterBomb(thrust, positionShotFrom, positionShotFrom.copy().sub(mouse).mag(), missileLevel, missileSize, clusterCannonLevel);
         b.sound = smallExplosion3;
         break;
       case RAILCANNON:
         b = new DefenceBomb(new PVector(0,0), mouse.copy(), 0f, missileLevel, missileSize);
         b.sound = smallExplosion2;
         break;
       case HOMINGCANNON:
         b = new HomingBomb(thrust, positionShotFrom, positionShotFrom.copy().sub(mouse).mag(), missileLevel, missileSize, homingCannonLevel);
         b.sound = smallExplosion1;
         break;
       default:
         return;
     }
     b.hasSound = true;
     objectsForCreation.add(b);
  }
  
  void upgradePower() {
   powerLevel*=1.15; 
  }
  
  void upgradeMissiles() {
   missileLevel*=1.2; 
  }
  
  void upgradeRateOfFire() {
   rateOfFireLevel*=1.1; 
  }
  
  void upgradeMissileSize() {
   missileSize*=1.15; 
  }
  
  void upgradeClusterMissiles() {
   clusterCannonLevel++; 
  }
  
  void upgradeHomingMissiles() {
    homingCannonLevel*=1.1;
  }
}