class Ball extends Entity {
  
  Ball() {
    velocity = new PVector(0,random(10));
    forces = new PVector(0,0);
    radius = 15f;
    mass = radius*PI*radius;
    position = new PVector(random(size),random(size));
    affectedByCollisions = true;
    affectedByGravity = true;
    affectedByDrag = true;
    enabled = true;
  }
  
  void render() {
    stroke(200);
    fill(150);
    ellipseMode(RADIUS);
    ellipse(position.x,position.y, radius, radius);
    fill(200);
    ellipse(position.x+radius*0.2,position.y-radius*0.2, radius*0.6, radius*0.6);
  }
  
  void runScripts() {}
}