class DefenceHomingThrust extends HomingThrust {
  
  DefenceHomingThrust(Entity s, float p, float r) {
   super(s,p,r);
  }
  
  boolean isEngagingForce(Entity o) {
    return (o.getClass() == EnemyBomb.class &&
          ((Bomb) o).renderState == ((Bomb) o).NORMALBOMB);
  }
}