class CityHomingThrust extends HomingThrust {
  
  CityHomingThrust(Entity s, float p, float r) {
   super(s,p,r);
  }
  
  boolean isEngagingForce(Entity o) {
    return (o.getClass() == City.class && o.enabled);
  }
}