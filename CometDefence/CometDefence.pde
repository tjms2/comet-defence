import java.util.Random;
import ddf.minim.*;

//cannon types
final int VANILLACANNON = 0;
final int CLUSTERCANNON = 1;
final int RAILCANNON = 2;
final int HOMINGCANNON = 3;

//enemy bomb types
final int RED = 0;//normal
final int BLUE = 1;//avoider
final int GREEN = 2;//homes on cities
final int PINK = 3;//heavy

Random r = new Random();

ArrayList<Entity> objects  = new ArrayList<Entity>();
ArrayList<Entity> objectsForDestruction;
ArrayList<Entity> objectsForCreation  = new ArrayList<Entity>();
ArrayList<ForceGenerator> activeForces = new ArrayList<ForceGenerator>();
ArrayList<ForceGenerator> forcesResolved;
ArrayList<AudioPlayer> currentSounds = new ArrayList<AudioPlayer>();

Entity ground;
Cannon cannon;
final static float size = 800f;

//level stuff
int bullets;
int bulletsFired;
int cometsDeterred;
int gold;
int score;
int health;
int powerUpgrade;
int ROFUpgrade;
int bombUpgrade;
int clusterCannonUpgrade;
int railCannonUpgrade;
int homingCannonUpgrade;


void setup() {
  frameRate(120);
  size(800, 900);
  setUpInitialGame();
  setUpLevelController();
}


void draw() {

  objectsForDestruction = new ArrayList<Entity>();
  forcesResolved = new ArrayList<ForceGenerator>();


  for (int i=0; i < objects.size(); i++) {
    objects.get(i).resetForce();
  }

  for (int i=0; i < activeForces.size(); i++) {
    activeForces.get(i).apply();
  }


  for (int i=0; i < objects.size(); i++) {
    objects.get(i).calculateAcceleration();//calculate acceleration off forces
  }

  for (int i=0; i < objects.size(); i++) {
    objects.get(i).updateVelocity();//update the velocity based on the acceleration
  }

  for (int i=0; i < objects.size(); i++) {
    if (objects.get(i).awake())
      objects.get(i).updatePosition();//add the velocity to the position of each object
  }

  for (int i=0; i < objects.size(); i++) {
    objects.get(i).runScripts();
  }

  resolveCollisions();
  resolveCollisions();
  resolveCollisions();
  resolveCollisions();
  resolveCollisions();
  resolveCollisions();

  renderBackground(); 

  for (int i=0; i < objects.size(); i++) {
    objects.get(i).render();//display each object, however render is also used for times scripts etc.
  }
  runLevelController();

  for (int i=0; i < objectsForDestruction.size(); i++) {
    objects.remove(objectsForDestruction.get(i));
  }

  for (int i=0; i < forcesResolved.size(); i++) {
    activeForces.remove(forcesResolved.get(i));
  }

  ArrayList<Entity> buffer = new ArrayList<Entity>();
  for (int i=0; i < objectsForCreation.size(); i++) {
    objects.add(objectsForCreation.get(i));
    buffer.add(objectsForCreation.get(i));
  }

  for (int i=0; i < buffer.size(); i++) {
    objectsForCreation.remove(buffer.get(i));
  }
}

ArrayList<Collision> collisions;
void resolveCollisions() {
  collisions = new ArrayList<Collision>();
  for (int i = 0; i < objects.size(); i++) {
    Entity o = objects.get(i);
    if (o.affectedByCollisions && o.enabled)
      for (int j = 0; j < objects.size(); j++) {
        Entity o2 = objects.get(j);
        if (o!=o2 && o2.affectedByCollisions && o2.enabled)
          if (o.touching(o2))
            if (o.approaching(o2)) 
              if (!collisionRegistered(o, o2))
                collisions.add(new Collision(o, o2, 0.95f));
      }
  }
  for (int i = 0; i < collisions.size(); i ++) collisions.get(i).resolve();
}

boolean collisionRegistered(Entity o1, Entity o2) {
  for (int i = 0; i < collisions.size(); i++) {
    if (collisions.get(i).is(o1, o2)) return true;
  }
  return false;
}

void mousePressed() {
  mouseDown(new PVector(mouseX, mouseY));
}



void setUpPhysics() {
  activeForces.add(new Gravity());
  activeForces.add(new Drag());
}

private PVector grav = new PVector(0, 0.01);
PVector getGravitationalAcceleration() {
  return grav.copy();
}

float getDragRate() {
  return 0.995f;
}

boolean offScreenHorizontally(PVector pos, float rad) {
  return (pos.x < -rad || pos.x > size+rad);
}

Minim m;
//sounds are downloaded off of royalty free sound distribution websites: soundbible.com https://www.freesoundeffects.com
AudioPlayer smallExplosion1;
AudioPlayer smallExplosion2;
AudioPlayer smallExplosion3;
AudioPlayer bigExplosion;
AudioPlayer gameOverExplosion;

//songs are all property of TheFatRat, with Monody featuring Laura Brehm
//downloaded off of arcadium.net , a website owned by TheFatRat and his label, that exists to provide content creators with music as long as they cite his authorship: https://the-arcadium.net/about
AudioPlayer timeLapse;//Music: TheFatRat - Time Lapse https://lnk.to/tfrtimelapse
AudioPlayer Monody;//Music: TheFatRat - Monody (feat. Laura Brehm) https://open.spotify.com/track/3VvBPkc24zC7x05mgJTyGO
AudioPlayer Unity;//Music: TheFatRat - Unity https://lnk.to/tfrunity
AudioPlayer Prelude;//Music: TheFatRat - Prelude VIP https://lnk.to/tfrpreludevip
AudioPlayer infinitePowerDrop;//Music: TheFatRat - Infinite Power //https://www.youtube.com/watch?v=3aLyiI2odhU
AudioPlayer infinitePower;//Music: TheFatRat - Infinite Power //https://www.youtube.com/watch?v=3aLyiI2odhU

void loadSound() {
  m = new Minim(this);
  bigExplosion = m.loadFile("Grenade Explosion-SoundBible.com-2100581469.mp3");//taken from http://soundbible.com/grab.php?id=1467&type=mp3
  smallExplosion1 = m.loadFile("Explosion+1.mp3");//taken from https://www.freesoundeffects.com/mp3_466446.mp3
  smallExplosion2 = m.loadFile("Gun+357+Magnum 1 short.mp3");//taken from https://www.freesoundeffects.com/mp3_466445.mp3
  smallExplosion3 = m.loadFile("Gun+1.mp3");//taken from https://www.freesoundeffects.com/mp3_466514.mp3
  gameOverExplosion = m.loadFile("Explosion+6.mp3");//taken from https://www.freesoundeffects.com/mp3_466451.mp3
  timeLapse = m.loadFile("TheFatRat+-+Time+Lapse.mp3");//taken from https://d3cj65qhk7sagp.cloudfront.net/tracks/Time+Lapse/TheFatRat+-+Time+Lapse.mp3
  Monody = m.loadFile("TheFatRat+-+Monody+feat.+Laura+Brehm+(Instrumental).mp3");//taken from https://d3cj65qhk7sagp.cloudfront.net/tracks/Monody/TheFatRat+-+Monody+(feat.+Laura+Brehm).mp3
  Unity = m.loadFile("TheFatRat+-+Unity.mp3");//taken from https://d3cj65qhk7sagp.cloudfront.net/tracks/Unity/TheFatRat+-+Unity.mp3
  Prelude = m.loadFile("TheFatRat+-+Prelude+(VIP+Edit+feat.+JJD).mp3");//taken from https://d3cj65qhk7sagp.cloudfront.net/tracks/Prelude/TheFatRat+-+Prelude.mp3
  infinitePowerDrop = m.loadFile("Infinite+-+Power+-+Drop.mp3");
  infinitePower = m.loadFile("TheFatRat+-+Infinite+Power.mp3");//taken from https://d3cj65qhk7sagp.cloudfront.net/tracks/Infinite+Power/TheFatRat+-+Infinite+Power.mp3
}