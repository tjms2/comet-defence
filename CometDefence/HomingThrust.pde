abstract class HomingThrust extends ForceGenerator {
  
  abstract boolean isEngagingForce(Entity o);
  
  Entity source;
  float power;
  float range;
  
  HomingThrust(Entity s, float p, float r) {
   source = s;
   power = p;
   range = r;
  }
  
  void apply() {
    boolean objFound=false;
    float dist = 0;
    Entity interactingObj = source;//initialised to source to get it to compile.
    for(int i = 0; i < objects.size(); i ++ ){
      Entity o = objects.get(i);
      if(isEngagingForce(o)) {
        float distToO = source.position.copy().sub(o.position).mag();
        if(distToO < homingRadius()) {
          if(objFound) {
            if(distToO < dist) {
              interactingObj = o;
              dist = distToO;
            }
          } else {
            objFound = true;
            dist = distToO;
            interactingObj = o;
          }
        }
      }
   }
   if(interactingObj != source) //dont do this if it is the source
     applyWith(interactingObj);//apply with the nearest interacting object
  }
  
  void applyWith(Entity o) {
   PVector relativePos = source.position.copy().sub(o.position);
   float relativePosMag = relativePos.mag();
   float forceMag = (relativePosMag - homingRadius())*power;//affects everything within radius*20 distance
   PVector f = relativePos.normalize().mult(forceMag);
   source.forces.add(f);
  }
  
  float homingRadius() {
   return range; 
  }
}