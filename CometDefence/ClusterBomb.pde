class ClusterBomb extends DefenceBomb {
    
  int clusters;
  
  ClusterBomb(PVector thrust, PVector position, float distanceToMouse, float bombLevel, float r, int children) {
    super(thrust, position, distanceToMouse, bombLevel*0.75, r);
    clusters = children;
  }
  
  void runScripts(){
    if(detonate()&&renderState==NORMALBOMB) {
      exploded = true;
      for(int i = 0; i < clusters; i++) {
        float angle = random(-PI,PI);
        Bomb b = new TimeBomb(position.copy().add(PVector.fromAngle(angle).mult(radius*5)), (int)random(20,20+clusters*3), bombLevel, radius*0.8);
        b.mass*=40;
        b.affectedByCollisions = false;
        objectsForCreation.add(b);
      }
    }
    super.runScripts();
  }
}