class Gravity extends ForceGenerator {
   
  
  void apply() {
    for(int i = 0; i < objects.size(); i ++) {
      Entity o = objects.get(i);
      if(o.affectedByGravity && o.enabled) {
        o.forces.add(gravitationalForce(o)); 
      }
    }
  }
  
  PVector gravitationalForce(Entity o) {
   return getGravitationalAcceleration().mult(o.mass); 
  }
}