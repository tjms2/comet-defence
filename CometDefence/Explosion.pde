class Explosion extends ForceGenerator {
 
 Entity source; 
 float p;
  //explosion affects everything within 
 Explosion(Entity o, float power) {
   source = o;
   p = power;
 }
  
 void apply() {
   for(int i = 0; i < objects.size(); i ++ ){
    Entity o = objects.get(i);
    if(o!= source && o != ground)
      if(o.affectedByExplosions)
        if(source.position.copy().sub(o.position).mag() < explosionSize())
          applyTo(o);   
   }
 }
 
 
 void applyTo(Entity o) {
   PVector relativePos = source.position.copy().sub(o.position);
   float relativePosMag = relativePos.mag();
   float forceMag = (relativePosMag - explosionSize())*p;//affects everything within radius*4 distance
   PVector f = relativePos.normalize().mult(forceMag);
   o.forces.add(f);
 }
 
 float explosionSize() {
   return source.radius*15; 
 }
}